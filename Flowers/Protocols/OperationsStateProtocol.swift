//
//  OperationsStateProtocol.swift
//  Flowers
//
//  Created by Admin on 26.07.17.
//  Copyright © 2017 windscape. All rights reserved.
//

import Foundation

protocol OperationStateDelegate: class {
    func operationFinished()
    func operationFinishedWith(error: String)
}
