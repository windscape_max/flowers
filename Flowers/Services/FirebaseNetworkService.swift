//
//  FirebaseNetworkService.swift
//  Flowers
//
//  Created by Admin on 17.07.17.
//  Copyright © 2017 windscape. All rights reserved.
//

import Foundation
import Firebase
import SVProgressHUD

class FirebaseNetworkService {
    
    weak var delegate: OperationStateDelegate?
    
    private lazy var ref = Database.database().reference()
    private lazy var storageRef = Storage.storage().reference()
    
    init() { }

    func listenData(completion: @escaping (_ result: Any?) -> Void) {
        weak var weakSelf = self
        
        ref.observe(.value, with: { (snapshot) in
            if snapshot.value != nil {
                completion(snapshot.value)
                weakSelf?.delegate?.operationFinished()
            } else {
                completion(nil)
                weakSelf?.delegate?.operationFinished()
            }
        }) { (error) in
            weakSelf?.delegate?.operationFinishedWith(error: error.localizedDescription)
        }
    }
    
    func addNewPost(image: UIImage, flowerData: Dictionary<String, Any>) {
        weak var weakSelf = self
        ref.child(flowerData["name"]! as! String).observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.exists() {
                weakSelf?.delegate?.operationFinishedWith(error: "This name is busy. Please, type other name.")
            } else {
                var flowers = flowerData
                
                let imageRef = weakSelf?.storageRef.child("\(String(describing: flowerData["name"]!)).jpg")
                let data = UIImageJPEGRepresentation(image, 0.1)
                
                let uploadTask = imageRef?.putData(data!, metadata: nil) { (metadata, error) in
                    guard metadata != nil else { return }
                }
                
                uploadTask?.observe(.progress) { snapshot in
                    SVProgressHUD.showProgress(Float(100.0 * Double(snapshot.progress!.completedUnitCount) / Double(snapshot.progress!.totalUnitCount)))
                }
                
                uploadTask?.observe(.success) { (FIRStorageTaskSnapshot) in
                    if let url = FIRStorageTaskSnapshot.metadata?.downloadURL() {
                        flowers["url"] = String(describing: url)
                    }
                    
                    SVProgressHUD.show()
                    weakSelf?.ref.child(flowers["name"]! as! String).setValue(flowers)
                    SVProgressHUD.dismiss()
                    weakSelf?.delegate?.operationFinished()
                }
                
                uploadTask?.observe(.failure) { (FIRStorageTaskSnapshot) in
                    if let error = FIRStorageTaskSnapshot.error {
                        weakSelf?.delegate?.operationFinishedWith(error: error.localizedDescription)
                    }else {
                        weakSelf?.delegate?.operationFinished()
                    }
                }
            }
        })
    }
    
    func deletePostWith(id: String, completion: (()->())?) {
        weak var weakSelf = self
        ref.child(id).removeValue { (error, FIRDatabaseReference) in
            if error != nil {
                weakSelf?.delegate?.operationFinishedWith(error: (error?.localizedDescription)!)
            }
        }
        
        weakSelf?.storageRef.child("\(id).jpg").delete(completion: { (error) in
            if error != nil {
                weakSelf?.delegate?.operationFinishedWith(error: (error?.localizedDescription)!)
            }
            
            if completion != nil {
                completion!()
            }
        })
    }
    
    func updateImage(image: UIImage, model: FlowerModel) {
        weak var weakSelf = self
        var dictModel = ModelParser.instance.getDictionaryFrom(model: model)
        let imageRef = weakSelf?.storageRef.child("\(model.uniqueID!).jpg")
        let data = UIImageJPEGRepresentation(image, 0.1)
        
        let uploadTask = imageRef?.putData(data!, metadata: nil) { (metadata, error) in
            guard metadata != nil else { return }
        }
        
        uploadTask?.observe(.progress) { snapshot in
            SVProgressHUD.showProgress(Float(100.0 * Double(snapshot.progress!.completedUnitCount) / Double(snapshot.progress!.totalUnitCount)))
        }
        
        uploadTask?.observe(.success) { (FIRStorageTaskSnapshot) in
            if let url = FIRStorageTaskSnapshot.metadata?.downloadURL() {
                dictModel["url"] = String(describing: url)
            }
            
            SVProgressHUD.show()
            weakSelf?.ref.child(model.uniqueID).updateChildValues(dictModel)
            SVProgressHUD.dismiss()
            weakSelf?.delegate?.operationFinished()
        }
        
        uploadTask?.observe(.failure) { (FIRStorageTaskSnapshot) in
            if let error = FIRStorageTaskSnapshot.error {
                weakSelf?.delegate?.operationFinishedWith(error: error.localizedDescription)
            }else {
                weakSelf?.delegate?.operationFinished()
            }
        }
    }
    
    func updatePostInfo(model: FlowerModel) {
        weak var weakSelf = self
        let dictModel = ModelParser.instance.getDictionaryFrom(model: model)
    
        SVProgressHUD.show()
        weakSelf?.ref.child(model.uniqueID).updateChildValues(dictModel, withCompletionBlock: { (error, FIRDatabaseReference) in
            if error != nil {
                SVProgressHUD.dismiss()
                weakSelf?.delegate?.operationFinishedWith(error: (error?.localizedDescription)!)
            }
            SVProgressHUD.dismiss()
            weakSelf?.delegate?.operationFinished()
        })
    }
}

