//
//  RegistrationViewController.swift
//  Flowers
//
//  Created by Admin on 18.07.17.
//  Copyright © 2017 windscape. All rights reserved.
//

import UIKit
import FirebaseAuth
import KeychainSwift

class RegistrationViewController: UIViewController, UITextFieldDelegate {

    private lazy var keychain = KeychainSwift()
    
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var registrateButton: UIButton!
    @IBOutlet weak var informationLabel: UILabel!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    private var activeField: UITextField?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupController()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    // MARK: - Settings
    func setupController() {
        name.delegate = self
        email.delegate = self
        password.delegate = self
        self.informationLabel.text = ""
        self.registrateButton.layer.cornerRadius = 6
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    // MARK: - Actions
    @IBAction func registrateAction(_ sender: UIButton) {
        if isAllFieldsCorrect() {
            Auth.auth().createUser(withEmail: email.text!, password: password.text!) { (user, error) in
                if error != nil {
                    self.showRegistrationErrorAlert(error: (error?.localizedDescription)!)
                }else {
                    self.keychain.set(user!.uid, forKey: "token")
                    self.navigateApp()
                }
            }
        }else {
            self.informationLabel.fadeInAndOutWith(text: "Please, fill all fields.", fadeInDuration: 1, fadeOutDuration: 1, fadeOutDelay: 3)
        }
    }
    
    // MARK: - Custom methods
    private func isAllFieldsCorrect() -> Bool {
        if name.text != "" && email.text != "" && password.text != "" {
            return true
        }
        return false
    }
    
    private func navigateApp() {
        if keychain.get("token") == Auth.auth().currentUser?.uid {
            let vc = storyboard?.instantiateViewController(withIdentifier: "mainNavigationVC") as! UINavigationController
            self.present(vc, animated: true, completion: nil)
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    private func showRegistrationErrorAlert(error: String) {
        let alert = UIAlertController(title: "Registration Error", message: "Error: \(error)", preferredStyle: .alert)
        let actionOK = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(actionOK)
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func keyboardWasShown(notification: NSNotification){
        //Need to calculate keyboard exact size due to Apple suggestions
        self.scrollView.isScrollEnabled = true
        var info = notification.userInfo!
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize!.height, 0.0)
        
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if let activeField = self.activeField {
            if (!aRect.contains(activeField.frame.origin)){
                self.scrollView.scrollRectToVisible(activeField.frame, animated: true)
            }
        }
    }
    
    @objc func keyboardWillBeHidden(notification: NSNotification){
        //Once keyboard disappears, restore original positions
        var info = notification.userInfo!
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, -keyboardSize!.height, 0.0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        self.view.endEditing(true)
        self.scrollView.isScrollEnabled = false
    }


    
    // MARK: - UITextFieldDelegate
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        activeField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField){
        activeField = nil
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == name {
            email.becomeFirstResponder()
        } else if textField == email {
            password.becomeFirstResponder()
        } else if textField == password {
            self.registrateAction(self.registrateButton)
        }
        
        return true
    }
}
