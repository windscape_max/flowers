//
//  DetailViewController.swift
//  Flowers
//
//  Created by Admin on 28.08.17.
//  Copyright © 2017 windscape. All rights reserved.
//

import UIKit
import SVProgressHUD
import CPImageViewer

private let cellId = "detailCellId"

class DetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UINavigationControllerDelegate, UIImagePickerControllerDelegate, CPImageControllerProtocol, OperationStateDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    private var editRightBarButtonItem: UIBarButtonItem!
    private var addRightBarButtonItem: UIBarButtonItem!
    private var airDropRightBarButtonItem: UIBarButtonItem!
    
    private var imageTapGesture: UITapGestureRecognizer!
    
    @IBOutlet weak var topSubviewHeightConstr: NSLayoutConstraint!
    @IBOutlet weak var topSubview: DesignableView!
    @IBOutlet weak var flowerImg: UIImageView!
    @IBOutlet weak var flowerName: UITextField!
    @IBOutlet weak var flowerDetail: UITextView!
    @IBOutlet weak var changeImgButton: UIButton!
    @IBOutlet weak var changeImgHeight: NSLayoutConstraint!
    @IBOutlet weak var deletePostButton: UIButton!
    @IBOutlet weak var deletePostHeight: NSLayoutConstraint!
    
    private var isEditingMode = false
    private var imagePicked = false
    
    private lazy var prevName = ""
    private lazy var prevDetail = ""
    private lazy var prevComment = ""
    
    var model: FlowerModel!
    
    private lazy var firebaseNetwork = FirebaseNetworkService()
    
    // MARK: - CPImageControllerProtocol
    var animationImageView: UIImageView!
    
    /// The viewer style. Defaults to Presentation
    public var viewerStyle = CPImageViewerStyle.presentation

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    private func setup() {
        
        imageTapGesture = UITapGestureRecognizer(target: self, action: #selector(imageTapped(_:)))
        flowerImg.isUserInteractionEnabled = true
        flowerImg.addGestureRecognizer(imageTapGesture)
        
        editRightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "edit"), style: .plain, target: self, action: #selector(editInfo))
        addRightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "plus"), style: .plain, target: self, action: #selector(addNewNote))
        airDropRightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(shareFlowerImage))
        
        self.navigationItem.rightBarButtonItems = [airDropRightBarButtonItem, addRightBarButtonItem, editRightBarButtonItem]
        
        model.delegate = self
        
        if model.flowerPhoto == nil {
            model.downloadImageWith(imageURL: model.url)
        } else {
            flowerImg.image = model.flowerPhoto
        }
        
        flowerName.text = model.flowerName
        flowerDetail.text = model.flowerDescription
        
        tableView.register(UINib.init(nibName: "DetailTableViewCell", bundle: nil), forCellReuseIdentifier: cellId)
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView(frame: .zero)
        
        flowerImg.layer.borderWidth = 0.5
        flowerImg.layer.borderColor = UIColor.lightGray.cgColor
        flowerImg.layer.cornerRadius = 6
        
        flowerDetail.layer.borderWidth = 0.5
        flowerDetail.layer.borderColor = UIColor.lightGray.cgColor
        flowerDetail.layer.cornerRadius = 6
        
        flowerName.layer.borderWidth = 0.5
        flowerName.layer.borderColor = UIColor.lightGray.cgColor
        flowerName.layer.cornerRadius = 6
        
        changeImgButton.layer.borderWidth = 0.5
        changeImgButton.layer.borderColor = UIColor.lightGray.cgColor
        changeImgHeight.constant = 0
        changeImgButton.setTitle("", for: .normal)
        
        deletePostButton.layer.borderWidth = 0.5
        deletePostButton.layer.borderColor = UIColor.lightGray.cgColor
        deletePostHeight.constant = 0
        deletePostButton.setTitle("", for: .normal)
        
        topSubviewHeightConstr.constant = self.view.bounds.height * 0.25
    }
    
    private func enableEditingMode() {
        editRightBarButtonItem.image = #imageLiteral(resourceName: "save")
        addRightBarButtonItem.isEnabled = false
        
        prevName = flowerName.text!
        prevDetail = flowerDetail.text!
        
        flowerDetail.layer.borderWidth = 1
        flowerDetail.layer.borderColor = UIColor.lightGray.cgColor
        flowerDetail.isUserInteractionEnabled = true
        
        flowerName.layer.borderWidth = 1
        flowerName.layer.borderColor = UIColor.lightGray.cgColor
        flowerName.isUserInteractionEnabled = true
        
        UIView.animate(withDuration: 0.7, animations: {
            self.changeImgHeight.constant = 28
            self.deletePostHeight.constant = 28
            self.topSubviewHeightConstr.constant = self.view.bounds.height * 0.4
            self.view.layoutIfNeeded()
            self.isEditingMode = !self.isEditingMode
        }, completion: { (Bool) in
            UIView.animate(withDuration: 0.25, animations: {
                self.changeImgButton.setTitle("Change image", for: .normal)
                self.deletePostButton.setTitle("Delete post", for: .normal)
                self.view.layoutIfNeeded()
            })
        })
    }
    
    private func disableEditingMode() {
        changeImgButton.setTitle("", for: .normal)
        deletePostButton.setTitle("", for: .normal)
        
        editRightBarButtonItem.image = #imageLiteral(resourceName: "edit")
        addRightBarButtonItem.isEnabled = true
        
        flowerDetail.layer.borderWidth = 0.5
        flowerDetail.layer.borderColor = UIColor.lightGray.cgColor
        flowerDetail.isUserInteractionEnabled = false
        
        flowerName.layer.borderWidth = 0.5
        flowerName.layer.borderColor = UIColor.lightGray.cgColor
        flowerName.isUserInteractionEnabled = false
        
        UIView.animate(withDuration: 0.7) {
            self.changeImgHeight.constant = 0
            self.deletePostHeight.constant = 0
            self.topSubviewHeightConstr.constant = self.view.bounds.height * 0.25
            self.view.layoutIfNeeded()
            self.isEditingMode = !self.isEditingMode
        }
        
        if imagePicked {
            model.flowerName = flowerName.text!
            model.flowerDescription = flowerDetail.text!
            firebaseNetwork.updateImage(image: flowerImg.image!, model: model)
            imagePicked = false
        } else if prevName != flowerName.text! || prevDetail != flowerDetail.text! {
            model.flowerName = flowerName.text!
            model.flowerDescription = flowerDetail.text!
            firebaseNetwork.updatePostInfo(model: model)
        }
    }
    
    @objc private func editInfo() {
        
        if (flowerName.text?.isEmpty)! || flowerDetail.text.isEmpty {
            let alert = UIAlertController(title: "Field are empty!", message: "Flower name and description cant be empty.", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
        } else {
            if isEditingMode {
                disableEditingMode()
            } else {
                enableEditingMode()
            }
        }
        
    }
    
    @objc private func addNewNote() {
        weak var weakSelf = self
        
        let alert = UIAlertController(title: "Write your comment", message: "", preferredStyle: .alert)
        
        let action = UIAlertAction(title: "Add comment", style: .default) { (alertAction) in
            let textField = alert.textFields![0] as UITextField
            if !(textField.text?.isEmpty)! {
                weakSelf?.model.comments.append(textField.text!)
                weakSelf?.firebaseNetwork.updatePostInfo(model: (weakSelf?.model)!)
                weakSelf?.tableView.reloadData()
            }
        }
        
        alert.addTextField { (textField) in
            textField.placeholder = "Type your comment"
        }
        alert.addAction(action)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    private func editCommentAt(_ index: Int) {
        weak var weakSelf = self
        
        let alert = UIAlertController(title: "Edit your comment", message: "", preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        let action = UIAlertAction(title: "Save", style: .default) { (alertAction) in
            let textField = alert.textFields![0] as UITextField
            
            if !(textField.text?.isEmpty)! {
                weakSelf?.model.comments[index] = textField.text!
                weakSelf?.firebaseNetwork.updatePostInfo(model: (weakSelf?.model)!)
                weakSelf?.tableView.reloadData()
            } else {
                let emptyFieldAlert = UIAlertController(title: "Empty field!", message: "Field cant be empty!", preferredStyle: .alert)
                let ok = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                emptyFieldAlert.addAction(ok)
                weakSelf?.present(emptyFieldAlert, animated: true, completion: nil)
            }
        }
        
        alert.addTextField { (textField) in
            textField.text = weakSelf?.prevComment
            textField.placeholder = "Edit your comment"
        }
        alert.addAction(action)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc private func shareFlowerImage() {
        let image = flowerImg.image
        
        let imageToShare = [ image! ]
        
        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
        
        activityViewController.popoverPresentationController?.sourceView = self.view
        activityViewController.excludedActivityTypes = [UIActivityType.airDrop]
        
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    private func openLibrary() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary;
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    private func openCamera() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera;
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }

    
    // MARK: - Actions
    
    @IBAction func imageTapped(_ sender: UITapGestureRecognizer) {
        let controller = CPImageViewerViewController()
        controller.transitioningDelegate = CPImageViewerAnimator()
        controller.image = flowerImg.image
        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func changeImgAction(_ sender: UIButton) {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let alert = UIAlertController(title: "", message: "Do you want open camera or library?", preferredStyle: .actionSheet)
            
            let openCameraAction = UIAlertAction(title: "Camera", style: .default, handler: { (UIAlertAction) in
                self.openCamera()
            })
            let openLibraryAction = UIAlertAction(title: "Library", style: .default, handler: { (UIAlertAction) in
                self.openLibrary()
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: { (UIAlertAction) in
                self.dismiss(animated: true, completion: nil)
            })
            
            alert.addAction(openCameraAction)
            alert.addAction(openLibraryAction)
            alert.addAction(cancelAction)
            
            self.present(alert, animated: true, completion: nil)
            
        } else if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let alert = UIAlertController(title: "Camera isn't available!", message: "Sorry, camera isn't avalible. Do u want open library?", preferredStyle: .alert)
            let openLibraryAction = UIAlertAction(title: "Open library", style: .default, handler: { (UIAlertAction) in
                self.openLibrary()
            })
            let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            
            alert.addAction(cancel)
            alert.addAction(openLibraryAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func deletePostAction(_ sender: UIButton) {
        weak var weakSelf = self
        let alert = UIAlertController(title: "Delete post", message: "Do you want delete this post?", preferredStyle: .alert)
        let yes = UIAlertAction(title: "Yes", style: .destructive) { (UIAlertAction) in
            SVProgressHUD.show()
            weakSelf?.firebaseNetwork.deletePostWith(id: (weakSelf?.model.uniqueID)!) {
                SVProgressHUD.dismiss()
                weakSelf?.navigationController?.popViewController(animated: true)
            }
        }
        let cancel = UIAlertAction(title: "No", style: .cancel, handler: nil)
        
        alert.addAction(yes)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - OperationStateDelegate
    
    func operationFinished() {
        DispatchQueue.main.async {
            self.flowerImg.image = self.model.flowerPhoto
            self.tableView.reloadData()
        }
    }
    
    func operationFinishedWith(error: String) {
        self.present(AlertsHelper.instance.showAlertWith(title: "Error", message: error), animated: true, completion: nil)
    }
    
    // MARK: - UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.comments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! DetailTableViewCell
        
        cell.commentLabel.text = model.comments[indexPath.row]
        
        return cell
    }
    
    // MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        if indexPath.row != 0 {
            print("indexPath.row")
            let cell = tableView.cellForRow(at: indexPath) as! DetailTableViewCell
            prevComment = cell.commentLabel.text!
            editCommentAt(indexPath.row)
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if indexPath.row == 0 || isEditingMode {
            return false
        }
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            model.comments.remove(at: indexPath.row)
            firebaseNetwork.updatePostInfo(model: model)
            tableView.reloadData()
        }
    }
    
    // MARK: - UIImagePickerControllerDelegate
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let info = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imagePicked = true
            flowerImg.image = info
        }
        self.dismiss(animated: true, completion: nil);
    }
}

@IBDesignable
class DesignableView: UIView {
}

extension UIView {
    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable
    var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable
    var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable
    var shadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }
}
