//
//  AddingFlowerVC.swift
//  Flowers
//
//  Created by Admin on 25.07.17.
//  Copyright © 2017 windscape. All rights reserved.
//

import UIKit

class AddingFlowerVC: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, OperationStateDelegate {
  
    @IBOutlet weak var flowerPhoto: UIImageView!
    @IBOutlet weak var takePhotoButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var descriptionTextField: UITextView!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    private var imagePicked = false
    
    private lazy var firebaseNetworkService = FirebaseNetworkService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    private func setupUI() {
        self.takePhotoButton.layer.cornerRadius = 6
        self.saveButton.layer.cornerRadius = 6
        
        self.descriptionTextField.layer.borderWidth = 1.5
        self.descriptionTextField.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        self.descriptionTextField.layer.cornerRadius = 6
        
        self.flowerPhoto.layer.borderWidth = 1.5
        self.flowerPhoto.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        self.flowerPhoto.layer.cornerRadius = 6
        
        self.datePicker.layer.borderWidth = 1.5
        self.datePicker.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        self.datePicker.layer.cornerRadius = 6
    }

    
    // MARK: - Actions
    @IBAction func saveAction(_ sender: UIButton) {
        if validateView() {
            firebaseNetworkService.delegate = self
            firebaseNetworkService.addNewPost(image: flowerPhoto.image!, flowerData: prepareData())
            self.view.isUserInteractionEnabled = false
        }        
    }
    @IBAction func getImageAction(_ sender: UIButton) {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let alert = UIAlertController(title: "", message: "Do you want open camera or library?", preferredStyle: .actionSheet)
            
            let openCameraAction = UIAlertAction(title: "Camera", style: .default, handler: { (UIAlertAction) in
                self.openCamera()
            })
            let openLibraryAction = UIAlertAction(title: "Library", style: .default, handler: { (UIAlertAction) in
                self.openLibrary()
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: { (UIAlertAction) in
                self.dismiss(animated: true, completion: nil)
            })
            
            alert.addAction(openCameraAction)
            alert.addAction(openLibraryAction)
            alert.addAction(cancelAction)
            
            self.present(alert, animated: true, completion: nil)
            
        } else if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let alert = UIAlertController(title: "Camera isn't available!", message: "Sorry, camera isn't avalible. Do u want open library?", preferredStyle: .alert)
            let openLibraryAction = UIAlertAction(title: "Open library", style: .default, handler: { (UIAlertAction) in
                self.openLibrary()
            })
            let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            
            alert.addAction(cancel)
            alert.addAction(openLibraryAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    // MARK: - Custom Methods
    private func prepareData() -> Dictionary<String, Any> {
        var dictionary = Dictionary<String, Any>()
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy:MM:dd"

        var comments: [String] = []
        comments.append("Flower added at \(formatter.string(from: datePicker.date))")
        
        dictionary["name"] = nameTextField.text!
        dictionary["description"] = descriptionTextField.text!
        dictionary["date"] = formatter.string(from: datePicker.date)
        dictionary["url"] = ""
        dictionary["comments"] = comments
        dictionary["uniqueID"] = nameTextField.text!
        return dictionary
    }
    
    private func openLibrary() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary;
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    private func openCamera() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera;
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    private func validateView() -> Bool {
        if !imagePicked {
            self.present(AlertsHelper.instance.showAlertWith(title: "Choose Image", message: "Please, choose flower image!"), animated: true, completion: nil)
            return false
        }
        if nameTextField.text == "" || descriptionTextField.text == "" {
            self.present(AlertsHelper.instance.showAlertWith(title: "Empty Fileds", message: "Please, type flower name and description"), animated: true, completion: nil)
            return false
        }
        
        if datePicker.date > Date() {
            self.present(AlertsHelper.instance.showAlertWith(title: "Date", message: "Please, choose right date!"), animated: true, completion: nil)
            return false
        }
        return true
    }
    
    // MARK: - FirebaseNetworkServiceDelegate
    
    func operationFinished() {
        self.view.isUserInteractionEnabled = true
        self.navigationController?.popViewController(animated: true)
    }
    
    func operationFinishedWith(error: String) {
        self.view.isUserInteractionEnabled = true
        self.present(AlertsHelper.instance.showAlertWith(title: "Error", message: error), animated: true, completion: nil)
    }
    
    // MARK: - UIImagePickerControllerDelegate
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let info = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imagePicked = true
            flowerPhoto.image = info
        }
        
        self.dismiss(animated: true, completion: nil);
    }

}
