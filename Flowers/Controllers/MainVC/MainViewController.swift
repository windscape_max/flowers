//
//  MainViewController.swift
//  Flowers
//
//  Created by Admin on 17.07.17.
//  Copyright © 2017 windscape. All rights reserved.
//

import UIKit
import Firebase
import KeychainSwift

class MainViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, OperationStateDelegate {

    @IBOutlet weak var addPhoto: UIButton!

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var topSubview: UIView!
    
    private lazy var firebaseNetworkingService = FirebaseNetworkService()
    
    var model: [FlowerModel] = []
    private var filteredModels: [FlowerModel] = []
    
    private var searchBar: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        firebaseNetworkingService.delegate = self
        
        firebaseNetworkingService.listenData { (value) in
            if let value = value as? Dictionary<String, Any> {
                self.model = ModelParser.instance.getModelsFrom(snapshot: value)
                self.filteredModels = self.model
                for i in self.model {
                    i.delegate = self
                }
            } else {
                self.model = []
                self.filteredModels = []
            }
        }
        
        searchBar = UISearchBar()
        searchBar.delegate = self
        searchBar.barTintColor = UIColor.groupTableViewBackground
        searchBar.tintColor = UIColor.lightGray
        searchBar.sizeToFit()

        self.tableView.tableHeaderView = searchBar
        self.tableView.register(UINib.init(nibName: "MainTableViewCell", bundle: nil), forCellReuseIdentifier: "mainTableViewCellIdentificator")
        self.tableView.tableFooterView = UIView(frame: .zero)
        
        self.view.bringSubview(toFront: topSubview)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        self.tableView.reloadData()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func exitAction(_ sender: Any) {
        let keychain = KeychainSwift()
        
        try? Auth.auth().signOut()
        keychain.clear()
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "authNavigationVC")
        self.present(vc!, animated: true, completion: nil)
    }
    // MARK: - UISearchBarDelegate
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if let searchText = searchBar.text {
            if searchText.isEmpty {
                filteredModels = model
            } else {
                filteredModels = []
                for i in model {
                    if (i.flowerName.range(of: searchText, options: [.caseInsensitive], range: nil, locale: nil) != nil) {
                        filteredModels.append(i)
                    }
                }
            }
            tableView.reloadData()
        }
    }
    
    // MARK: - FirebaseNetworkServiceDelegate
    
    func operationFinished() {
        DispatchQueue.main.async{
            self.tableView.reloadData()
        }       
    }
    
    func operationFinishedWith(error: String) {
        self.view.isUserInteractionEnabled = true
        self.present(AlertsHelper.instance.showAlertWith(title: "Error", message: error), animated: true, completion: nil)
    }
    
    // MARK: - UITableViewDelegate & Data Source

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredModels.count == 0 ? 0 : filteredModels.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "mainTableViewCellIdentificator", for: indexPath) as! MainTableViewCell
        
        if let flowerPhoto = filteredModels[indexPath.row].flowerPhoto {
            cell.flowerImage.image = flowerPhoto
        }
        
        cell.title.text = filteredModels[indexPath.row].flowerName
        cell.flowerDescription.text = filteredModels[indexPath.row].flowerDescription

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "detailVC") as! DetailViewController
        vc.model = filteredModels[indexPath.row]
        tableView.deselectRow(at: indexPath, animated: true)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if filteredModels.count != model.count {
            return false
        }
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            firebaseNetworkingService.deletePostWith(id: model[indexPath.row].uniqueID, completion: nil)
            model.remove(at: indexPath.row)
            tableView.reloadData()
        }
    }
}
