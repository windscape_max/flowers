//
//  AuthenticationViewController.swift
//  Flowers
//
//  Created by Admin on 17.07.17.
//  Copyright © 2017 windscape. All rights reserved.
//

import UIKit
import FirebaseAuth
import KeychainSwift



class AuthenticationViewController: UIViewController, UITextFieldDelegate {

    private lazy var keychain = KeychainSwift()
    
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var informationLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    
    private var activeField: UITextField?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    // MARK: - Setup
    func setup() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        self.loginButton.layer.cornerRadius = 6
        self.signupButton.layer.cornerRadius = 6
        
        email.delegate = self
        password.delegate = self
    }

    // MARK: - Actions
    @IBAction func loginAction(_ sender: UIButton) {
        
        if isAllFieldsCorrect() {
            Auth.auth().signIn(withEmail: email.text!, password: password.text!, completion: { (user, error) in
                if error != nil {
                    self.showLoginErrorAlert(error: (error?.localizedDescription)!)
                }else {
                    self.keychain.set(user!.uid, forKey: "token")
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "mainNavigationVC") as! UINavigationController
                    self.present(vc, animated: true, completion: nil)
                }
            })
        }else {
            self.informationLabel.fadeInAndOutWith(text: "Please, fill all fields.", fadeInDuration: 1, fadeOutDuration: 1, fadeOutDelay: 3)
        }
    }
    
    @IBAction func signupAction(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "registrationVC") as! RegistrationViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }

    // MARK: - Custom methods
    private func showLoginErrorAlert(error: String) {
        let alert = UIAlertController(title: "Registration Error", message: "Error: \(error)", preferredStyle: .alert)
        let actionOK = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(actionOK)
        self.present(alert, animated: true, completion: nil)
    }

    private func isAllFieldsCorrect() -> Bool {
        if email.text != "" && password.text != "" {
            return true
        }
        return false
    }
    
    @objc func keyboardWasShown(notification: NSNotification){
        //Need to calculate keyboard exact size due to Apple suggestions
        self.scrollView.isScrollEnabled = true
        var info = notification.userInfo!
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize!.height, 0.0)
        
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if let activeField = self.activeField {
            if (!aRect.contains(activeField.frame.origin)){
                self.scrollView.scrollRectToVisible(activeField.frame, animated: true)
            }
        }
    }
    
    @objc func keyboardWillBeHidden(notification: NSNotification){
        //Once keyboard disappears, restore original positions
        var info = notification.userInfo!
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, -keyboardSize!.height, 0.0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        self.view.endEditing(true)
        self.scrollView.isScrollEnabled = false
    }
    
    // MARK: - UITextFieldDelegate   
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        activeField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField){
        activeField = nil
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == email {
            password.becomeFirstResponder()
        }else if textField == password {
            loginAction(self.loginButton)
        }        
        return true
    }
    
}
