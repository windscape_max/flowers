//
//  FlowerModel.swift
//  Flowers
//
//  Created by Admin on 26.07.17.
//  Copyright © 2017 windscape. All rights reserved.
//

import Foundation
import UIKit

class FlowerModel {
    
    weak var delegate: OperationStateDelegate?
    
    var flowerName: String!
    var flowerDescription: String!
    var comments: [String] = []
    var url: String!
    var date: String!
    var uniqueID: String!
    var flowerPhoto: UIImage?
    
    init(flowerName: String, flowerDescription: String, comments: [String], date: String, url: String, uniqueID: String) {
        self.flowerName = flowerName
        self.flowerDescription = flowerDescription
        self.comments = comments
        self.date = date
        self.url = url
        self.uniqueID = uniqueID
        downloadImageWith(imageURL: url)
    }
    
    func downloadImageWith(imageURL: String) {
        let url = URL(string: imageURL)
        weak var weakSelf = self
        DispatchQueue.global().async {
            let data = try? Data(contentsOf: url!)
            
            if data != nil {
                if let image = UIImage(data: data!) {
                    self.flowerPhoto = image
                    weakSelf?.delegate?.operationFinished()
                }
            } else {
                self.flowerPhoto = #imageLiteral(resourceName: "flower")
            }
        }
    }
}
