//
//  UILabel+Fade.swift
//  Flowers
//
//  Created by Admin on 18.07.17.
//  Copyright © 2017 windscape. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    
    func fadeInAndOutWith(text: String?, fadeInDuration: TimeInterval, fadeOutDuration: TimeInterval, fadeOutDelay: TimeInterval) {
        if text != nil && text != "" {
            self.text = text
        }
        self.alpha = 0
        UIView.animate(withDuration: fadeInDuration, animations: { () -> Void in
            self.alpha = 1
        }) { (Bool) -> Void in
            
            // After the animation completes, fade out the view after a delay
            UIView.animate(withDuration: fadeOutDuration, delay: fadeOutDelay, options: [.curveEaseOut], animations: { () -> Void in
                self.alpha = 0
            }, completion: nil)
        }
    }
    
    func fadeWith(text: String?, fadeDuration: TimeInterval, fromAlpha: CGFloat, toAlpha: CGFloat){
        if text != nil && text != "" {
            self.text = text
        }
        self.alpha = fromAlpha
        UIView.animate(withDuration: fadeDuration) {
            self.alpha = toAlpha
        }
    }
}
