//
//  UIScrollView+ResignTextField.swift
//  Flowers
//
//  Created by Admin on 12.09.17.
//  Copyright © 2017 windscape. All rights reserved.
//

import Foundation
import UIKit

extension UIScrollView {
    open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for i in self.subviews {
            if i is UITextField {
                i.resignFirstResponder()
            }
        }
    }
}
