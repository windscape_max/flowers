//
//  MainTableViewCell.swift
//  Flowers
//
//  Created by Admin on 24.07.17.
//  Copyright © 2017 windscape. All rights reserved.
//

import UIKit

class MainTableViewCell: UITableViewCell {

    @IBOutlet weak var flowerImage: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var flowerDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func prepareForReuse() {
        self.flowerImage.image = nil
        self.title.text = ""
        self.flowerDescription.text = ""
    }
    
}
