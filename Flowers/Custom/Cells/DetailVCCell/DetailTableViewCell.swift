//
//  DetailTableViewCell.swift
//  Flowers
//
//  Created by Admin on 29.08.17.
//  Copyright © 2017 windscape. All rights reserved.
//

import UIKit

class DetailTableViewCell: UITableViewCell {

    @IBOutlet weak var commentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        commentLabel.text = ""
    }
    
}
