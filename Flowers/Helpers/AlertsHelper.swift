//
//  AlertsHelper.swift
//  Flowers
//
//  Created by Admin on 25.07.17.
//  Copyright © 2017 windscape. All rights reserved.
//

import Foundation
import UIKit

class AlertsHelper {
    
    static let instance = AlertsHelper()
    
    private init() {}
    
    func showAlertWith(title: String, message: String) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(ok)
        return alert
    }

}
