//
//  ModelParser.swift
//  Flowers
//
//  Created by Admin on 26.07.17.
//  Copyright © 2017 windscape. All rights reserved.
//

import Foundation
import Firebase

class ModelParser {

    static let instance = ModelParser()
    
    private init() {}
    
    func getModelsFrom(snapshot: Any?) -> [FlowerModel] {
        var flowerModelArray: [FlowerModel] = []
        
        if let snapshot = snapshot as? Dictionary<String, Any> {
            for i in snapshot {
                let value = i.value as! Dictionary<String, Any>
                let model = FlowerModel(flowerName: value["name"] as! String, flowerDescription: value["description"] as! String, comments: value["comments"] as! [String],
                                        date: value["date"] as! String, url: value["url"] as! String, uniqueID: value["uniqueID"] as! String)
                flowerModelArray.append(model)
            }
        }
        return flowerModelArray
    }
    
    func getDictionaryFrom(model: FlowerModel) -> Dictionary<String, Any> {
        var dictionary:  Dictionary<String, Any> = [:]
        
        dictionary["name"] = model.flowerName
        dictionary["description"] = model.flowerDescription
        dictionary["date"] = model.date
        dictionary["url"] = model.url
        dictionary["comments"] = model.comments
        dictionary["uniqueID"] = model.uniqueID
        //dictionary[model.flowerName] = ["comments" : model.comments, "date" : model.date, "description" : model.flowerDescription, "url" : model.url, "name" : model.flowerName]
        
        return dictionary
    }

}
